#include <LiquidCrystal.h>
 
const int lcdColumns = 16;
const int lcdRows = 2;

unsigned long globaltime = 0;
long time; // counts 1/100ths of seconds
char timeString[6]; // Holds time string for LCD

LiquidCrystal lcd(8, 11, 4, 5, 6, 7);

#define LCD_ENABLE_PIN 11
#define LCD_ENABLE_PROTECTION_PIN 10
#define SENSOR_PIN 14
#define SENSOR_PROTECTION_PIN 15

#define MOTOR_IN_1 17
#define MOTOR_IN_2 16

String pinNames[] = {"", "", "Pause btn", "Calibration btn",
                     "LCD Data 4", "LCD Data 5", "LCD Data 6", "LCD Data 7",
                     "LCD RS pin", "Speaker pin", "LCD_E_P pin", "LCD Enable pin",
                     "", "", "Sensor pin", "SENSOR_P pin", "Motor In 1", "Motor In 2"};
String pinColors[] = {"", "", "Blue", "Blue",
                     "Orange", "Orange", "Green", "Green",
                     "Purple", "Grey", "Yellow", "Blue",
                     "", "", "Purple", "Orange", "Brown", "White"};


void setup() {
  Serial.begin(9600);
  pinMode(LCD_ENABLE_PROTECTION_PIN, OUTPUT);
  digitalWrite(LCD_ENABLE_PROTECTION_PIN, HIGH);
  pinMode(SENSOR_PROTECTION_PIN, OUTPUT);
  digitalWrite(SENSOR_PROTECTION_PIN, HIGH);
  
  lcd.begin(lcdColumns, lcdRows); //LCD dimension: 16x2(Coluns x Rows)
  globaltime = millis();
}

void loop() {  
  sprintf(timeString, "%1d.%02ds", time/100, time%100); // Format time string
  lcd.setCursor(lcdColumns - (sizeof(timeString)-1), 1); // Positions the cursor on bottom right corner
  lcd.print(timeString); // Write the time

  // Replace this by using millis() and globaltime
  delay(10); // Waits for 1/100th of a second
  time += 1; // Increment time counter

  // Check errors every second
  if (time % 100 == 0)
    errorDetect();
}

void errorDetect() {
  
  bool success = false;
  bool foundError = false;
  while (!success)
  {
    digitalWrite(SENSOR_PROTECTION_PIN, LOW);
    digitalWrite(LCD_ENABLE_PROTECTION_PIN, LOW);
    digitalWrite(MOTOR_IN_1, LOW);
    digitalWrite(MOTOR_IN_2, LOW);
    // Assume success
    success = true;

    int errorPin = 0;
    // Loop over pins
    for (int i = 2; i < 18; i++) {
      if (i == 12 || i == 13)
        continue;
      // Set pinMode to pullup & check    
      pinMode(i, INPUT_PULLUP);
      if (digitalRead(i) == HIGH){
        Serial.println(i);
        errorPin = i;
        success = false;
        // We don't need to check for other wires
        break;
      }
      // Reset pinMode of this pin
      if (i == SENSOR_PIN)
        pinMode(i, INPUT);
      else
        pinMode(i, OUTPUT);

      // Make sure the all essential pins stay low (and the motor stays off)
      if (i == SENSOR_PROTECTION_PIN || i ==  LCD_ENABLE_PIN || i == LCD_ENABLE_PROTECTION_PIN || i == MOTOR_IN_1 || i == MOTOR_IN_2)
        digitalWrite(i, LOW);
    }
    // Set transistor protection pins back to normal
    pinMode(LCD_ENABLE_PROTECTION_PIN, OUTPUT);
    pinMode(SENSOR_PROTECTION_PIN, OUTPUT);
    digitalWrite(SENSOR_PROTECTION_PIN, HIGH);
    digitalWrite(LCD_ENABLE_PROTECTION_PIN, HIGH);
    if (!success) {
      foundError = true;
      
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(pinNames[errorPin]);
      lcd.setCursor(0,1);
      lcd.print("Error: " + pinColors[errorPin]);
      
      // Error beep -> low pitch
      tone(9, 1480, 500);
      delay(1000);      
    } else { // All pins are OK
      if (foundError) {
        // RE-INITIALIZE LCD HERE!!
        // Haven't had the time to test this yet.
        // Probably:
        // lcd = new LiquidCrystal(8, 11, 4, 5, 6, 7);
        // or:
        // Last beep -> higher pitch
        tone(9, 2000, 800);
        delay(1000);

        lcd.begin(lcdColumns, lcdRows); //LCD dimension: 16x2(Coluns x Rows)
      }
      // Exit while loop
      break;
    }
  }
}
