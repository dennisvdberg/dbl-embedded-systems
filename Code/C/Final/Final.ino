// Here we include the library we need for the LCD
#include <LiquidCrystal.h>

/* --------------------------------------
*              Pin definitions
* --------------------------------------- */

// These are the pins we use on the Arduino, to be set for modes later
// We use #define instead of const int pin = 1, since this speeds up the eventual code

#define pausePin       2
#define calibrationPin 3
#define LCD_D4_Pin     4
#define LCD_D5_Pin     5
#define LCD_D6_Pin     6
#define LCD_D7_Pin     7
#define LCD_RS_Pin     8
#define speakerPin     9
#define LCD_E_P_Pin    10
#define LCD_E_Pin      11
#define motor_checker  12
#define sensorPin      14
#define sensor_P_Pin   15
#define motor_In1_Pin  16
#define motor_In2_Pin  17
#define button_checker 18

// These are non-existing Arduino pins,
// but we need to number them for error detection
// and as such, need to be defined as well.

#define motor_In1_con  20
#define motor_In2_con  21

#define sensorSensitivity 0.12
#define motorTurningDelay 80
#define lcdColumns 16
#define lcdRows 2

/* ------------------------------------
*           Variable definitions
* --------------------------------------- */

// Normal non-constant variables that we use in the process

bool isFinished             = false;
bool isPaused               = false;
bool isOff                  = true;
bool isAborted              = false;

bool needCalibration        = true;
bool isSettingInterrupts    = false;

bool avoidDoublePause       = false;
bool avoidDoubleCalibration = false;

float neutralValue = 3.75;

int loopCounter = 0;
int stopWatch = 0;
char timeString[7]; // Holds time string for LCD
unsigned long globalTime = millis();

// constant variables we use in the process

const String pinNames[] = {"", "", "Pause btn", "Calibration btn",
                     "LCD Data 4", "LCD Data 5", "LCD Data 6", "LCD Data 7",
                     "LCD RS pin", "Speaker pin", "LCD_E_P pin", "LCD Enable pin",
                     "Motor chkr pin", "", "Sensor pin", "SENSOR_P pin", "Motor In 1", "Motor In 2",
                     "Button checker", "", "Motor In1 contr.", "Motor In2 contr."};
const String pinColors[] = {"", "", "Blue", "Blue",
                     "Orange", "Orange", "Green", "Green",
                     "Purple", "Grey", "Yellow", "Blue",
                     "Green", "", "Purple", "Orange", "Brown", "White",
                     "Yellow", "", "Brown", "White"};

const int inputPins[] = {motor_checker, pausePin, calibrationPin, sensorPin};

// definition of the lcd

LiquidCrystal lcd(LCD_RS_Pin, LCD_E_Pin, LCD_D4_Pin, LCD_D5_Pin, LCD_D6_Pin, LCD_D7_Pin);

/* ------------------------------------
*               Program code
* --------------------------------------- */

void setup() {

  // The setup function is a single function that runs once as defined in Arduino API.
  // This function is used to set the pinModes, however since we need to set the pinModes later,
  // we build a function called setPinModes that sets the pins to the correct modes.
  // Now it just sets it for the main part.

  Serial.begin(9600);
  setPinModes();

  digitalWrite(LCD_E_P_Pin, HIGH);
  digitalWrite(sensor_P_Pin, HIGH);
  digitalWrite(button_checker, HIGH);

  lcd.begin(lcdColumns, lcdRows); //LCD dimension: 16x2(Coluns x Rows)

  setInterrupts(true);
}

void loop() {
  // The loop that gets called over and over again
  // this is the body of our program
  // To start, we read the value from the sensor by on of the helperFunctions readSensor
  float readValue = readSensor();

  Serial.println( readValue );

  if( isAborted ) {
    // When the program is aborted, we clear the lcd and write Aborted
    // Then with the current running time
    // after 3 seconds, we just turn the machine off so that we can start over
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Aborted!   ");

    writeTime();

    delay(3000);

    isAborted  = false;
    isPaused   = false;
    isOff      = true;
    globalTime = millis();
  }

  // This is to make sure that we know that we can keep sorting as per the State-Machine.
  // Now, as long as it is not finished, paused, off and it does not need Calibration, then it can sort
  bool isSorting = !isFinished && !isPaused && !isOff && !needCalibration;

  // If we are sorting, and we passed 3 whole loops,
  //     we skip 2 loops of testing to make sure that it is not lagging.
  // We can start whether errors occur
  // This, we also do when we are not sorting, as then we might be sorting because of the fact that errors exist
  if( (isSorting && loopCounter > 3) || !isSorting ) {
    detectErrors();
    isSorting   = !isFinished && !isPaused && !isOff && !needCalibration;
    loopCounter = 0;
  }

  if( isSorting ) {

    lcd.setCursor(0,0);
    lcd.print("Sorting..");

    unsigned long time = (unsigned long)(millis() - globalTime);
    globalTime         = millis();
    stopWatch         += (int)((time%1000)/10);

    writeTime();

    // Check where the values lie,
    // If this is below the interval of the neutralValue, it's a black disk
    // If this is above the interval of the neutralValue, it's a white disk
    // When this is in the interval, check 15 times with small delays (100 ms) whether it still remains there
    // If so, we can say that it is finished
    if( readValue < neutralValue - sensorSensitivity ) {
        Serial.println("Black");
        setMotor(true);
        delay(motorTurningDelay);
    } else if( readValue > neutralValue + sensorSensitivity ) {
        Serial.println("White");
        setMotor(false);
        delay(motorTurningDelay);
    } else {
        setMotor(false, true);
        Serial.println("No disc");
        // Assume finished
        isFinished = true;
        for (int i = 0; i <= 15; i++) {
          if(readSensor() > neutralValue + sensorSensitivity ||
                readSensor() < neutralValue - sensorSensitivity ) {

            isFinished = false;
            break;
          }
          delay(100);
        }
    }
  } else if( !needCalibration ) {

    lcd.clear();

    if( isOff ) {
      // The Machine is turned off
      stopWatch = 0;
      lcd.setCursor(0,0);
      lcd.print("Idle, waiting");
      lcd.setCursor(0,1);
      lcd.print("for input.");
    } else if( isFinished ) {
      // The machine has finished sorting
      lcd.setCursor(0,0);
      lcd.print("Finished    ");
      writeTime();
      delay(3000);
      isFinished = false;
      isOff      = true;
    } else if( isPaused ) {
      // The machine is now paused
      lcd.setCursor(0,0);
      lcd.print("Paused      ");
      writeTime();
    }

    delay(400);
    globalTime = millis();

  } else {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.println("Empty platform  ");
    lcd.setCursor(0,1);
    lcd.println("Press calibrate ");
    delay (1000);
    globalTime = millis();
  }

  loopCounter = loopCounter + 1;
}

/* --------------------------------------
*           Helper functions
* --------------------------------------- */

void calibrationInterrupt() {

  // make sure that we are not setting interupts
  if( isSettingInterrupts ) {
    isSettingInterrupts = false;
    return;
  }

  // Noise handling
  int highCounter = 0;

  for( int i = 0; i < 20; i++ ) {

    delayMicroseconds(10000); // 10ms

    if( digitalRead(calibrationPin) == HIGH ) {
      highCounter = highCounter + 1;
      i = 0;
    } else {
      highCounter = 0;
    }

    // Check if this interrupt might be fired due to a disconnected wire
    if( highCounter > 5 && !isValidInterrupt(calibrationPin) ) {
      Serial.println("Invalid calibration interrupt");
      setInterrupts(false);
      return;
    }
  }

  if( !isPaused && !isOff ) {
    return;
  }

  if( avoidDoubleCalibration ) {
    avoidDoubleCalibration = false;
    return;
  }

  avoidDoubleCalibration = true;

  if( !needCalibration ) {
    Serial.println("Calibration waiting");

    needCalibration = true;

  } else if( needCalibration ) {

    Serial.println("Calibration reading");

    float calibrationSum = 0;
    int timesToMeasure = 20;

    for (int i = 0; i < timesToMeasure; i++) {
      calibrationSum = calibrationSum + readSensor();
      delayMicroseconds(100000); // 100ms
    }

    neutralValue = calibrationSum / timesToMeasure;
    Serial.println("Calibrated on new neutral value:");
    Serial.println(neutralValue);
    needCalibration = false;
    lcd.clear();
  }
}

// The function that gets called when there is an interrupt concerning pause.
void pauseInterrupt() {

  if (isSettingInterrupts) {
    isSettingInterrupts = false;
    return;
  }

  setMotor(false, true);
  // Noise handling

  int highCounter = 0;

  for( int i = 0; i < 20; i++ ) {

    // As interrupts are unable to use the delay function, we'll use delayMicroseconds
    delayMicroseconds(10000); // 10ms

    if( digitalRead(pausePin) == HIGH ) {
      highCounter++;
      i = 0;
    } else {
      highCounter = 0;
    }

    if( highCounter > 5 ) {
      
      // Check if this interrupt might be fired due to a disconnected wire
      if( !isValidInterrupt(pausePin) ) {
        Serial.println("Invalid pause interrupt");
        setInterrupts(false);
        return;
      } else if( highCounter > 150 && isPaused && !needCalibration && !isAborted ) {
        isAborted = true;

        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Aborted!   ");
        writeTime();
      }
      
    }
  }

  if( needCalibration || isAborted ) {
    return;
  }

  Serial.println("PRESSED S/T BUTTON!");

  if( avoidDoublePause ) {
    avoidDoublePause = false;
    return;
  }

  avoidDoublePause = true;

  if( isOff ) {
    isOff = false;
  } else {
    isPaused = !isPaused;
  }

  lcd.clear();
}

void writeTime() {
  // Write the time it elapsed on the screen
  sprintf(timeString, "%2d.%02ds", stopWatch/100, stopWatch%100); // Format time string
  lcd.setCursor(lcdColumns - (sizeof(timeString)-1), 1); // Positions the cursor on bottom right corner
  lcd.print(timeString); // Write the time
}


// This function is an override function to make sure that we can set a default value for the off parameter
// when this function gets called with two parameters it will skip this function and move to the next one
// This is required for setting a default parameter value as the normal
//     void setMotor( bool reverse, bool off = false )
// Does not compile under Arduino C
void setMotor( bool reverse ) {
    setMotor( reverse, false );
}

void setMotor( bool reverse, bool off ) {
  // default parameter for "off" as sometimes we do not call it with that value set
  // Activates the motor and sets a direction
  digitalWrite(motor_In1_Pin, ! reverse);
  digitalWrite(motor_In2_Pin, reverse);

  if( off ) {
    digitalWrite(motor_In1_Pin, 0);
    digitalWrite(motor_In2_Pin, 0);
  }
}

float readSensor() {
    //reads the sensor and reports the voltage read
    int sensorValue = analogRead(sensorPin);
    float voltage   = sensorValue * (5.0/1023.0);

    return voltage;
}

void detectErrors() {
  Serial.println("checking for errors");
  bool success = false;
  bool foundError = false;
  while( !success ) {

    // Stop motors and disable some protection pins
    digitalWrite(sensor_P_Pin, LOW);
    digitalWrite(LCD_E_P_Pin, LOW);
    digitalWrite(motor_In1_Pin, LOW);
    digitalWrite(motor_In2_Pin, LOW);
    // Assume success
    success = true;

    int errorPin = 0;

    // Loop over pins
    for( int i = 2; i < 19; i++ ) {
      if( i == 13 ) {
        continue;
      }

      // Set pinMode to pullup & check
      pinMode(i, INPUT_PULLUP);
      // A HIGH reading means this pin cannot 'find' the ground
      // through a lower resistor than the input pullup-resistor.
      // Hence; it is not connected to the arduino.
      if( digitalRead(i) == HIGH ){
        // Save error pin number for displaying error
        errorPin = i;
        success = false;

        // We don't need to check for other wires
        if( errorPin != pausePin && errorPin != calibrationPin ) {
          break;
        }
      }

      // Check if motor is connected, using the motor_checker and motor input pins,
      // this is done separately from all other pins.
      // Note that the motor_checker pin works as an OR-gate for motor_In1_Pin and motor_In2_Pin.
      if( i == motor_checker ) {
        pinMode(i, INPUT);
        // Set one motor input pin to HIGH
        digitalWrite(motor_In1_Pin, HIGH);
        digitalWrite(motor_In2_Pin, LOW);

        // By design of the motor_checker pin
        // the motor_checker should read HIGH now.
        if( digitalRead(i) == LOW ) {
          errorPin = motor_In1_con;
          success = false;
        }

        // Rinse and repeat for the other input pin.
        digitalWrite(motor_In1_Pin, LOW);
        digitalWrite(motor_In2_Pin, HIGH);

        if( digitalRead(i) == LOW ) {
          errorPin = motor_In2_con;
          success = false;
        }

        // Make sure we stop the motor immediately after checking the pins.
        digitalWrite(motor_In1_Pin, LOW);
        digitalWrite(motor_In2_Pin, LOW);

        // If we have already found a disconnected pin, checking the rest is not necessary.
        if( !success ) {
          break;
        }
      }

      // Make sure the all essential pins stay low (and the motor stays off)
      if( i == speakerPin || i == sensor_P_Pin || i ==  LCD_E_Pin || i == LCD_E_P_Pin || i == motor_In1_Pin || i == motor_In2_Pin ) {
        digitalWrite(i, LOW);
      }
    }

    //Reset pinmodes
    setPinModes();

    // Set transistor protection pins back to normal
    digitalWrite(LCD_E_P_Pin, HIGH);
    digitalWrite(sensor_P_Pin, HIGH);
    digitalWrite(button_checker, HIGH);

    // Do error handling
    if( !success ) {
      Serial.println("Has error");
      foundError = true;
      isSettingInterrupts = true;

      // Disable interrupts while we have an error
      setInterrupts(false);

      // Display error message
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(pinNames[errorPin]);
      lcd.setCursor(0,1);
      lcd.print("Error clr " + pinColors[errorPin]);

      // Error beep -> low pitch
      tone(9, 1480, 500);
      delay(1000);
    } else { // All pins are OK
      if( foundError ) {
        Serial.println("Fixed error");

        // Last 'error fixed beep' -> high pitch
        tone(9, 2000, 800);
        delay(1000);

        // Re-initialize the LCD, just to be sure
        lcd.begin(lcdColumns, lcdRows); //LCD dimension: 16x2(Coluns x Rows)
        lcd.clear();

        // Re-enable interrupts
        setInterrupts(true);

        // Set the machine to paused if needed, we don't want to continue sorting immediately
        if( !needCalibration && !isOff ) {
          isPaused = true;
        }
      }
      // Exit while loop
      break;
    }
  }
}

void setPinModes() {
  // Reset pinModes
  for( int i = 0; i < 19; i++ ) {
    bool pinIsInputPin = false;
    for( int x = 0; x < sizeof(inputPins)/2; x++ ) {
      if (i == inputPins[x]) {
        pinIsInputPin = true;
        break;
      }
    }

    if( pinIsInputPin ) {
      pinMode(i, INPUT);
    } else {
      pinMode(i, OUTPUT);
    }
  }
}

bool isValidInterrupt( int pin ) {
  // Make sure we do not fire any interrupts now
  setInterrupts(false);

  bool isValid = false;
  digitalWrite(button_checker, LOW);
  // If the button pin is still high, it must be disconnected
  if( digitalRead(pin) == LOW ) {
    isValid = true;
  }
  digitalWrite(button_checker, HIGH);

  // Reset interrupts
  setInterrupts(true);
  return isValid;
}

void setInterrupts(bool enable) {
  // Set all the interrupts
  if (enable) {
    attachInterrupt( digitalPinToInterrupt(calibrationPin),
      calibrationInterrupt, HIGH);

    attachInterrupt( digitalPinToInterrupt(pausePin),
      pauseInterrupt, HIGH);
  } else {
    detachInterrupt(digitalPinToInterrupt(pausePin));
    detachInterrupt(digitalPinToInterrupt(calibrationPin));
  }
}
