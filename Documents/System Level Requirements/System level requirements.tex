The system level requirements is split up into three sub-chapters; the use-cases, user constraints and safety properties. The use-cases describe as to how the user is supposed to control and use the machine.
User constraints consist of rules the user must comply to in order to make the machine work properly. The safety properties are (generally technical) rules that should be uphold in order to ensure safety of the user and make sure the machine does not get damaged.

We declare a list of properties the machine should be able to do. These points can later be used for testing the machine.

\begin{enumerate}[label=SLR\arabic*]
	\item\label{SLR1} The machine will properly sort any number of black and white discs (feature \ref{F4}) the tube can hold.
	\item\label{SLR2} The machine will stop sorting after it has processed all discs in the tube and will notify the user.
	\item\label{SLR3} The machine should be able to detect if a wire (connected to the micro-controller) is broken or gets disconnected and give proper feedback to the user. A full description is given in feature \ref{F3}.
	\item\label{SLR4} The machine will pause and resume by means of a pause/resume button. When the machine is paused and the same button is hold down for $\pm 2$ seconds, the machine will abort all operations and return to its resting state and display an message on the \texttt{LCD}.
\end{enumerate}

\subsection{Use-cases} \label{SLR:Use_cases}
    The main use-case for our machine is sorting black and white discs. The user should connect the $9V$ power supply to the Arduino to power the machine. In order to make sure the sensor is properly calibrated in a new environment (room/lighting condition), the user should press the calibration button and follow the instructions on the \texttt{LCD} display (see section \ref{LCD_pinout} for more information on the \texttt{LCD}).

    After the machine finished calibration, the user fills the dedicated tube with discs and makes sure he abides to the user constraints (\ref{CS1}, \ref{CS2}, \ref{CS3} \& \ref{CS4}) set for this specific machine. Once the discs are in place, pressing the pause/resume button (see \ref{Buttons_explained}) will start the machine and initialize its sorting process.

    If the user wants to pause the sorting sequence he can do so by (shortly) pressing the pause/resume button. This will make the machine pause after finishing the current disc on the plate and put the machine in a pausing state. The user can make the machine continue by shortly pressing the button once more. If the pause/resume button is hold down for more than $2$ seconds while in its pausing state, the machine will abort the process all together and return to its resting state (\ref{SLR4} and \ref{SP6}) and display an message on the \texttt{LCD}.

    Should the machine experience any kind of disruption (e.g. bad cables) it will inform the user by using the display and making a beeping noise by using the speaker (feature \ref{F2}, see section \ref{Speaker_explained} for more about the speaker) next to the \texttt{LCD}. If a \texttt{LCD} wire is disconnected, the speaker will still be able to notify the user and the same for the symmetric case; if a speaker wire is disconnected and in fact; all --- non-trivial --- wires can be error detected and give the user proper feedback (feature \ref{F3} \& \ref{SLR3}). The machine will immediately stop and go to the pause state if the sorting sequence was currently running. The \texttt{LCD} will display what the current problem is and display the error (if it does not concern the \texttt{LCD} itself). The user can resume the sorting process after resolving the problem by means of the pause/resume button. The display will go back to it's original state and the speaker will make one more final longer beep and stop.

    When the machine runs out of discs it will stop automatically and inform the user if the operation was successful by using the \texttt{LCD} (\ref{SLR2}).

\subsection{User Constraints} \label{SLR:User_constraints}
    To make sure that our machine can operate quickly, but more importantly correctly we set up some user constraints the user must adhere to in order to guarantee proper results.
    \begin{enumerate}[label=UC\arabic*]
        \item\label{CS1} The user is expected to place the machine on a flat, leveled surface.
        \item\label{CS2} The user is expected to turn the sorting arm away from the tube and loading platform prior to turning on the machine and loading the discs.
        \item\label{CS3} The user is expected to put in all discs in the dedicated loading tube prior to starting the sorting sequence on the machine.
        \item\label{CS4} The user is expected to check the tube for any flipped discs that are positioned sideways, that is; vertically, and place the discs properly prior to starting the sorting sequence on the machine.
        \item\label{CS5} The user is expected to not touch the breadboard, that is; disconnect resistors, transistors, etcetera, in order to fulfill feature \ref{F3}.
        \item\label{CS6} The user is expected not to intervene with the sorting process in any way other than aborting or pausing the process by means of the pause/resume button. That is; the user should not put objects in front of the lamps, physically hold the motor or sorting arm, block the sensor, intervene with the disc tube, etcetera.
        \item\label{CS7} The user is expected not to touch the platform, lights, sensor or move any physical part of the machine after calibration in order to guarantee a successful sorting process.
    \end{enumerate}

\subsection{Safety Properties} \label{SLR:Safety_properties}
    \begin{enumerate}[label=SP\arabic*]
	    \item\label{SP1} Lamps must not be connected to more than 9V, otherwise they might burn out.
	    \item\label{SP2} The motor must be operating on 9V, because higher voltage could damage the motor. Lower voltages influence the strength and speed of the motors but won't cause any damage. See section \ref{L293D_pinout} for more information.
	    \item\label{SP3} The sensor is polarized so connecting it the wrong way could damage the transistor. See section \ref{Sensor_explained} on how to connect the sensor.
	    \item\label{SP4} When any of the wires to the \texttt{LCD} is connected to a voltage higher than 5V, the backlight, the brightness controller or internal transistors might burn out. See chapter \ref{LCD_pinout} on how to properly connect the \texttt{LCD}.
	    \item\label{SP5} Besides the lamp and the motor, the rest of the wires should not be connected to a voltage higher than 5V otherwise it can damage the functionality of the machine.
	    \item\label{SP6} After the pause/resume button is hold for at least two second while the machine is in its pausing state, the machine will stop instantaneously, as stated in \ref{SLR4}.
    \end{enumerate} 