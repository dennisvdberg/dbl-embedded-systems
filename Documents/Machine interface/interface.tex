In the machine interface we describe and explain how electronic parts and wires are connected to the Arduino such that all components work appropriately and the machine behaves according to the system level requirements.

\subsection{Motors} \label{L293D_pinout}
The Arduino is connected to \texttt{PCB}\footnote{Printed Circuit Board} which contains a \texttt{L293D} Motor driver integrated circuit which can be used to drive \texttt{DC} motors in either directions. The \texttt{L293D} has multiple connections, including; 
\begin{itemize}
	\item \texttt{Enable 1} (Enable motor 1 controls)
	\begin{itemize}
		\item \texttt{In 1} (\ref{pin:A2})
		\item \texttt{Out 1}
		\item \texttt{In 2} (\ref{pin:A3})
		\item \texttt{Out 2}
	\end{itemize}
	\item \texttt{Enable 2} (Enable motor 2 controls)
	\begin{itemize}
		\item \texttt{In 3}
		\item \texttt{Out 3}
		\item \texttt{In 4}
		\item \texttt{Out 4}
	\end{itemize}
	\item 4 Ground pins
	\item $+V_{board}$ ($5V$) (Power the circuit, connected to \ref{pin:5V})
	\item $+V_{motor}$ ($6V\sim9V$) (Power the motors, \ref{SP2})
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.75]{../includes/l293d}
	\caption{L293D pinout}
\end{figure}

The \texttt{PCB} is made such that $V_{motor}$, $V_{board}$ and the ground pins are connected without the need of additional wires. In order to control a motor, an \ref{pin:5V} pin should be connected to \texttt{Enable 1}.

Since the motor will always be enabled while sorting the discs, enabling motor 1 for the entirety of the sorting process makes sense and therefore, we have connected \texttt{Enable 1} to a \ref{pin:5V} steady output. However, at the end of the sort (or in the machine pause state), the motors needs to be turned off, which - if a steady \ref{pin:5V} is connected to the \texttt{Enable 1} pin - has to be done by setting \texttt{In 1} and \texttt{In 2} either \texttt{LOW} or \texttt{HIGH}. This will initially maintain the current and rapidly cause dynamic braking, which is used in the pause/resume and abort function described in \ref{SLR4}.

\texttt{OUT 1} and \texttt{OUT 2} are connected to the motor, and to two diodes which are connected to one resistor. This resistor is then connected to a base of a transistor. The collector of this transistor is connected to \ref{pin:5V} and the emitter to the \texttt{MOTOR\_PROTECTION\_PIN} (\ref{pin:D12}). Error detection using this set-up is explained in section \ref{Error_detection}.

After \texttt{Enable 1}, \texttt{OUT 1} and \texttt{OUT 2} are properly connected, we can control motor 1 using \texttt{In 1} and \texttt{In 2}. Hence; we connect two output pins to \texttt{In 1} and \texttt{In 2}. The circuit is designed such that sending a \texttt{HIGH} voltage ($5V$) on \texttt{In 1} and a \texttt{LOW} voltage ($0V$) on \texttt{In 2} will make the motor turn clockwise (making a disc end up in the right bucket) and likewise, switching the voltages on the \texttt{In}-pins will make the motor turn counter-clockwise.


\subsection{Sensor and light} \label{Sensor_explained}
The light sensor (or \texttt{LDR}) is connected to the \texttt{SENSOR\_PROTECTION\_PIN} (\ref{pin:A1}) on the anode, whereas the cathode is connected to an analogue input pin on the Arduino as well as to the ground by a $10$k$\Omega$ resistor in parallel with the Arduino pin (in this order according to \ref{SP3}). This creates a reference point and prevents under- or hypersensitivity of the sensor. The sensor can be error-detected similar to the \texttt{LCD} procedure (explained in section \ref{Error_detection}) using the \texttt{SENSOR\_PROTECTION\_PIN}.

The light is simply connected to a $9V$ (according to \ref{SP1}) pin and then straight to a ground on the Arduino.

\subsection{LCD} \label{LCD_pinout}
The \texttt{LCD} used is a default \texttt{1602A} $16 \times 2$ screen with $16$ pins. We will go over the pinout and how it is connected to the Arduino.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{../includes/lcd}
	\caption{1602A pinout}
\end{figure}

Going from left to right, we will describe the pinout of the \texttt{LCD}.

The \texttt{GND} is the ground pin, which needs to be connected to the ground of the Arduino. The \texttt{VCC} is the \ref{pin:5V} power supply of the \texttt{LCD}. The \texttt{VEE} controls the brightness of the screen, which is best connected to a potentiometer in order to gain control over the screen brightness. The \texttt{RS} pin is the register select pin, which controls to where in the \texttt{LCD}'s memory data has to be written to. For example; \texttt{LCD}'s instructions on what to do next or data to be displayed on the screen.

The \texttt{LCD} has a \texttt{R/W} pin that selects reading or writing mode. Then we have the enable pin which enables writing to \texttt{LCD} registers. We then arrive at $8$ data pins which correspond to $8$ bits (or one byte) of data, either for writing or reading. We only use \texttt{DB4} - \texttt{DB7}, since this is enough to write text to the screen. The last two pins control the \texttt{LED} back-light, where the \texttt{A} (sometimes; \texttt{BLA} or \texttt{LED+}) is the back-light anode, which can be connected to \ref{pin:5V} or through a resistor to darken the screen somewhat. We use a potentiometer to vary the brightness. The \texttt{K} (sometimes; \texttt{BLK} or \texttt{LED-}) is the back-light cathode, to be connected to the ground.

We connect the \texttt{RS}, enable, \texttt{DB4} up until \texttt{DB7} pins to the Arduino. The \texttt{R/W} pin can be set to the ground, since we only want to write to the screen. This configuration is sufficient to write text to the screen.

None of the wires is exposed to a higher voltage than \ref{pin:5V}, except for the lamp and the motor, according to safety property \ref{SP5}.

\subsection{Buttons \& peripherals}
\subsubsection{Buttons} \label{Buttons_explained}
We included two buttons, these will be connected to \ref{pin:A4} (the \texttt{BUTTONS\_PROTECTION\_PIN} pin, which has its \texttt{pinMode} set to \texttt{OUTPUT} and set to \texttt{HIGH}) and to an Arduino input pin, reading $5V$ whenever the button is pressed. The Arduino has two digital pins usable for interrupts, namely; pin \ref{pin:D2} and pin \ref{pin:D3}. We use one button, connected to pin \ref{pin:D2}, for pause/abort purposes (\ref{SLR4} and \ref{SP6}). We use another button to calibrate the machine, connected to the other interrupt pin (feature \ref{F1}). The buttons can be error detected similar to the \texttt{LCD} procedure (explained in section \ref{Error_detection}) using the \texttt{BUTTONS\_PROTECTION\_PIN}.

\subsubsection{Speaker} \label{Speaker_explained}
We added error detection on the \texttt{LCD}, however; if a \texttt{LCD} data pin is disconnected from the Arduino, there is no proper way to display the error on the \texttt{LCD}. We use a speaker to notify the user of an error regarding the \texttt{LCD}. One speaker pin is connected to an Arduino digital pin and the other to the ground. The Arduino pin is an output pin, sending the right frequencies to the speaker. This is related to feature \ref{F2}.

\subsection{Error detection} \label{Error_detection}
For error detection we designed an easy-to-use and stable electronic solution. Every once in a while, the software runs a check on all cables, setting the \texttt{pinMode} of all pins to be checked to \texttt{INPUT\_PULLUP} (IP).

The default \texttt{INPUT} mode sets a pin as an input, that is; if a pin is totally disconnected, it will \textit{float} and read \texttt{HIGH} and \texttt{LOW} randomly. If you connect it to $+5V$ or $0V$, it will obviously read \texttt{HIGH} or \texttt{LOW} respectively. The \texttt{IP} mode connects an internal pullup resistor of around $40$k$\Omega$, and the logic value read without any wires connected will be \texttt{HIGH}. We give a few examples as to how error detection will work on some peripheral devices:
\begin{enumerate}
	\item Speaker error handling
	
	The speaker only has one wire to be checked for. Once the check routine is called, the speaker pin is set to \texttt{IP} and current will flow through the speaker ($\sim 16\Omega$) to the ground\footnote{If this check would take to much time, the speaker would make sound. However, the $5V$ going through the speaker is for such small interval that no sound is to be heard.}. The speaker pin will read \texttt{LOW} and we know the wire is connected. If the wire is disconnected, the pin will read \texttt{HIGH}.
	
	\item Liquid Crystal Display enable pin error handling \label{LCD_EP_Error}
	
	This same technique can be used for any pin, however; some pins are puzzling to get working properly.
	
	Data pins are relatively easy. By default, setting data pins to \texttt{IP} wouldn't make sense, since it is not connected to the ground, and hence; it will always read \texttt{HIGH}. Connecting it in parallel straight to the ground would stop data from being received properly by the \texttt{LCD}. Connecting a high enough resistor ($10$k$\Omega$)in parallel to the ground solves this problem.
	
	There are however the more challenging pins, particularly; the enable pin. Whenever this pin is set to \texttt{HIGH}, data is read from the data pins and written in the \texttt{LCD}'s memory. That is; setting the enable pin to \texttt{IP} is no option, since it would write whatever is currently on the data pins to the address that is currently selected by the \texttt{RS} pin, resulting in the most interesting and eccentric characters on the display. In order to reliably check the enable pin, we have come up with the following solution;
	
	\begin{figure}[h]
		\makebox[\linewidth][c]{
			\begin{subfigure}[b]{.6\textwidth}
				\centering
				\includegraphics[width=.95\textwidth]{../includes/LCD_enable_schem}
				\caption{\texttt{LCD} Enable pin error detection schematics}
				\label{fig:LCD_enable_schem}
			\end{subfigure}
			\begin{subfigure}[b]{.6\textwidth}
				\centering
				\includegraphics[width=.95\textwidth]{../includes/LCD_enable_bb}
				\caption{\texttt{LCD} Enable pin error detection breadboard}
				\label{fig:LCD_enable_bb}
			\end{subfigure}
		}
		\caption{Isolated \texttt{LCD} Enable pin connections}
	\end{figure}
	
	In figure \ref{fig:LCD_enable_schem} we see two Arduino pins connected. We connect \ref{pin:D10} (Digital pin 10) to the base of a \texttt{NPN} transistor and \ref{pin:D11} to the collector of this transistor, this is the pin controlling the \texttt{LCD} enable input. The emitter is connected to the \texttt{LCD} enable pin. \ref{pin:D10} will serve as an extra pin for detecting errors on the enable pin, we name it: \texttt{LCD\_ENABLE\_PROTECTION\_PIN}. Both need to be in parallel to the ground by means of two resistors; \texttt{R1} \& \texttt{R2}, as can be seen in figure \ref{fig:LCD_enable_schem} and figure \ref{fig:Full_schem}, in order to detect errors when using IP, just like in the data pin example. We do not want to burn the transistor, so \ref{pin:D10} is connected to the base through a transistor as well (\texttt{R10}). The emitter is connected to the \texttt{LCD} and parallel to the ground  as well (using \texttt{R9}), considering the wire connected to the enable pin would otherwise float if the transistor is not connected (open). The resistor and pin names match the names in the full schematics, figure \ref{fig:Full_schem}.
	
	Using this configuration we can reliably test the enable pin. By default; pin \ref{pin:D10} is set to \texttt{HIGH} output, making the Arduino use \ref{pin:D11} as the enable pin, as if it were directly connected to the \texttt{LCD}. If we want to test the enable pin, \ref{pin:D10} is set to \texttt{LOW}, making the \texttt{LCD}'s enable pin input connected straight to the ground through \texttt{R9} and hence; reading \texttt{LOW} and not writing to the \texttt{LCD}'s memory. Now we can safely set \ref{pin:D11} to \texttt{IP} and test it, like usual. Now, since we have added one wire to the Arduino (\ref{pin:D10}), we also need to check this wire for disconnections. This can simply be done by setting \ref{pin:D11} to \texttt{LOW} and making the \texttt{pinMode} of \ref{pin:D10} \texttt{IP} after \ref{pin:D11} has been checked. Now, the transistor is closed, but \ref{pin:D11} is \texttt{LOW}, so the \texttt{LCD} will not write anything to memory. \ref{pin:D10} is now checked and set back to default (\texttt{OUTPUT}, \texttt{HIGH}). The program can continue executing after both checks.
	\item Motor output pins
	
	We use a transistor to check if the motor \texttt{IN 1} and \texttt{IN 2} pins are connected. The wires are connected as described in section \ref{L293D_pinout}. If we set both \texttt{IN 1} and \texttt{IN 2} to \texttt{LOW}, then the base of the transistor is \texttt{LOW} and \texttt{MOTOR\_PROTECTION\_PIN} will read \texttt{LOW}. If either \texttt{IN 1} or \texttt{IN 2} are set to \texttt{HIGH}, the \texttt{MOTOR\_PROTECTION\_PIN} will read \texttt{HIGH} (like an OR-gate) and hence; we can check if \texttt{IN 1} and \texttt{IN 2} are connected by setting one \texttt{LOW} and the other \texttt{HIGH} and then switch to check the other one. The \texttt{MOTOR\_PROTECTION\_PIN} is also connected to the ground by means of a resistor, this pin can be checked by using the default \texttt{IP} technique.
\end{enumerate}
This concludes the error detection and enables us to satisfy feature \ref{F3}.

\subsection{Pin connections}
\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|>{\centering\arraybackslash}p{6cm}|c|}
		\hline
		\multicolumn{2}{|c|}{\textbf{Pin}} & \textbf{Connection} & \textbf{Note} \\ \hline
		
		\multicolumn{2}{|c|}{\customlabel{pin:GND}{\texttt{GND}}} & Arduino ground & Used to ground multiple components \\ \hline
		\multicolumn{2}{|c|}{\customlabel{pin:5V}{$5V$}} & Arduino output power & Used to power multiple components \\ \hline
		
		\multirow{14}{*}{\begin{turn}{90}Digital Pins\end{turn}} 
		
		& \customlabel{pin:D0}{\texttt{D0}} & - & Used for serial communication \\ \cline{2-4}
		& \customlabel{pin:D1}{\texttt{D1}} & - & Used for serial communication \\ \cline{2-4}
		& \customlabel{pin:D2}{\texttt{D2}} & Pause/Abort button pin & See section \ref{Buttons_explained} \\ \cline{2-4}
		& \customlabel{pin:D3}{\texttt{D3}} & Calibration button pin & See section \ref{Buttons_explained} \\ \cline{2-4}
		& \customlabel{pin:D4}{\texttt{D4}} & \texttt{LCD} Data pin 4 & See section \ref{LCD_pinout} \\ \cline{2-4}
		& \customlabel{pin:D5}{\texttt{D5}} & \texttt{LCD} Data pin 5 & See section \ref{LCD_pinout} \\ \cline{2-4}
		& \customlabel{pin:D6}{\texttt{D6}} & \texttt{LCD} Data pin 6 & See section \ref{LCD_pinout} \\ \cline{2-4}
		& \customlabel{pin:D7}{\texttt{D7}} & \texttt{LCD} Data pin 7 & See section \ref{LCD_pinout} \\ \cline{2-4}
		& \customlabel{pin:D8}{\texttt{D8}} & \texttt{LCD} RS pin & See section \ref{LCD_pinout} \\ \cline{2-4}
		& \customlabel{pin:D9}{\texttt{D9}} & Speaker pin & See section \ref{Speaker_explained} \\ \cline{2-4}
		& \customlabel{pin:D10}{\texttt{D10}} & \texttt{LCD\_ENABLE\_PROTECTION\_PIN} & See example \ref{LCD_EP_Error} in section \ref{Error_detection} \\ \cline{2-4}
		& \customlabel{pin:D11}{\texttt{D11}} & \texttt{LCD} Enable pin & See section \ref{LCD_pinout} \\ \cline{2-4}
		& \customlabel{pin:D12}{\texttt{D12}} & \texttt{MOTOR\_PROTECTION\_PIN} & See section \ref{L293D_pinout} \\ \cline{2-4}
		& \customlabel{pin:D13}{\texttt{D13}} & - & This pin has no default \texttt{IP} mode \\ \hline
		
		\multirow{6}{*}[-0.4cm]{\begin{turn}{90}Analogue Pins\end{turn}} 
		
		& \customlabel{pin:A0}{\texttt{A0}} & Sensor pin & See section \ref{Sensor_explained} \\ \cline{2-4}
		& \customlabel{pin:A1}{\texttt{A1}} & \texttt{SENSOR\_PROTECTION\_PIN} & See section \ref{Sensor_explained} \\ \cline{2-4}
		& \customlabel{pin:A2}{\texttt{A2}} & Motor \texttt{In 1} pin & See section \ref{L293D_pinout} \\ \cline{2-4}
		& \customlabel{pin:A3}{\texttt{A3}} & Motor \texttt{In 2} pin & See section \ref{L293D_pinout} \\ \cline{2-4}
		& \customlabel{pin:A4}{\texttt{A4}} & \texttt{BUTTONS\_PROTECTION\_PIN} & See section \ref{Buttons_explained} \\ \cline{2-4}
		& \customlabel{pin:A5}{\texttt{A5}} & - & - \\ \cline{2-4}
		& \customlabel{pin:A6}{\texttt{A6}} & - & - \\ \hline
	\end{tabular}
	\caption{Arduino pin connections}
\end{table}

\subsection{Schematics}
\begin{figure}[H]
	\centering
	\includegraphics[width=.9\textwidth]{../includes/Full_schem}
	\caption{Full schematics}
	\label{fig:Full_schem}
\end{figure}

\subsection{Breadboard design}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{../includes/Full_bb}
	\caption{Full breadboard}
	\label{fig:Full_bb}
\end{figure}