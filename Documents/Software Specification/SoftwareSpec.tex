\section{Introduction}
Following the machine design, we set up how the software is going to be implemented. To do this we must understand exactly how our machine is going to operate. In this phase we will look at what all the inputs and outputs of the system are and most importantly how each interacts with one another. In this document, software specification of the machine, we will start with describing in natural language an overview of input and outputs. A state machine for the sorting machine will be constructed using UPPAAL. This state machine will be the basis for the software design and implementation, where we will program the code. \\
On this state machine in UPPAAL we also took some tests, which are used to proof requirements in the system validation and testing. These tests can be found in section \ref{UPPAAL tests}.


\section{Description of Inputs and Outputs}
    \subsection{Inputs:}
    \begin{itemize}
      \item The Start/Stop button:
            \begin{itemize}
              \item When the machine is in resting state, the pushing button will start it. If the machine is running, pressing the button will pause the machine. Then the user can either do a short press for less than 2 seconds to start the machine or a long press for more than 2 seconds to turn the machine off. Specified in \ref{SLR4}.
            \end{itemize}
      \item The white/black sensor:
            \begin{itemize}
              \item A sensor is used to detect whether the disc is black or white or whether no disc is present.
              \item In order to have a precise detection a lamp emits light on the disc, the intensity of the light reflected is received by the sensor, which sends the exact voltage intensity to the Arduino board.
            \end{itemize}
    \end{itemize}


    \subsection{Outputs:}
    \begin{itemize}
      \item A motor:
            \begin{itemize}
              \item The motor powers a pusher by turning a either clockwise or counterclockwise. This pusher is used to push the discs to their according bucket.
            \end{itemize}
      \item Lamp:
            \begin{itemize}
              \item Emits light on the disc to determine with a sensor the intensity of light reflected by the disc. The lamp is always on.
            \end{itemize}
      \item Display:
            \begin{itemize}
              \item The display is connected to the Arduino board which can show a message according to the state which the machine is in.
              \item The display will provide an error message when one of the wires is disconnected.
            \end{itemize}
      \item A speaker (see \ref{F2}):
            \begin{itemize}
              \item The speaker is used to give a feedback sound when an error occurs. This is needed to give feedback for an error with the display.
            \end{itemize}
    \end{itemize}

    \subsection{Relation between input and output}
    \begin{tabular}{|l|l|p{10cm}|}
      \hline
      % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
      Start/stop & Motor & If the machine is paused, the motor should stop turning once it has finished its current turn. If the machine is on, the motor can turn according to information from the sensor. \\ \hline
      Start/stop & Display & The display should notify the user if the machine is on or off. \\ \hline
      Sensor & Motor & The motor should turn according to the value of the sensor. Thus one way if a white value is detected, the other for black and it should not turn when no disc is found. \\ \hline
      Sensor & Display & When an error is found with the sensor, this should be displayed on the LCD screen. \\ \hline
      Sensor & Speaker & Idem \\ \hline
    \end{tabular}

  \section{Software Requirements}
  Correct operation of the machine requires correct software, to ensure correctness of the software this document contains the software requirements that have to be met. \\

  \begin{enumerate}[label=SLR\arabic*]
    \item\label{SR1} The machine will use sensor value to determine the correct way to set the motor, in order to have it turn the right way or stop.

    \item\label{SR2} The machine will use calibration to get the right thresholds for the sensor input to detect black, white or no discs.

    \item\label{SR3} The machine will detect when any cable is disconnected and send signal to the LCD and speaker. It will throw an error and the machine will stop turning.

    \item\label{SR4} The machine will respond to start/stop presses as specified in \ref{SLR4}.
\end{enumerate}

\newpage

  \section{Sketch State Machine}
  In order to make a working UPPAAL model for the software specification, we had to design a finite state automaton. We started by sketching the state machine, shown in figure \label{fig:UPPAAL model:Sketch}. In this sketch a counter is used to determine when the machine has come to an end, whenever it saw more than the max number of white/black disc the machine would stop. In the final UPPAAL model this is changed to have the sensor detect when no disc is present.

      \begin{figure}[h]
        \centering
		\includegraphics[scale=0.5]{../includes/automataembd}
		\caption{The first automata}
        \label{fig:UPPAAL model:Sketch}
	\end{figure}
	\begin{itemize}
	\item State $q_0$ is the initial state which checks if an error occurs, when it will move to the state E or if there is a signal from the sensor will go to the state called Sens
	\item State E is the state in which the error is identified and shown
	\item State Sens is the sensor phase in which it is checked whether there is a black disc (in which case, depending on the $bstate$ or $wstate$, it will move to the state $q_1$ $\vee$ $q_3$)
	\item State $q_1$ is the state in which we update the $counter$ to $counter++$ and check if the disc is black and that the $bstate$ is on
	\item State $q_2$ is the state in which we update the $counter$ with $counter++$ and check if the disc is white and the $wstate$ is on
	\item State $q_3$ is the state in which the $wstate$ is set to false and the $bstate$ to true and set the counter to 0
	\item State $q_4$ is the state in which the $wstate$ is set to true and the $bstate$ to false and set the counter to 0
	\item State $q_t$ in which we check if the counter $\leq$ tresh
	\item State $q_5$ in which it will be decided that the pusher will rotate to the right
	\item State $q_6$ in which it will be decided that the pusher will rotate to the left
	\end{itemize}

\newpage
	
\section{UPPAAL}\label{sec:uppaal}
When designing the automata for the software specification of our machine, we made use of four automata: a main automaton, an on/off automaton, an user automaton and an error automaton. The automata can 'communicate' with each other through synchronization channels, which are used to synchronize transitions between automata.

	\subsection{Global declarations} \label{Declaration}
	When building the automata in UPPAAL we make use of several global variables. In UPPAAl we declare these variables in the global declarations section. We use two clocks, one for timing the turning and waiting in the main automaton, the other for timing the pause/abort function of \ref{SLR4}. We use $sensor$ integer to give an abstract representation of the sensor input, where 0,1,2 are black, white, and no disc respectively. Delay is used for the time the machine turns in one run, and for the time it waits. We use a boolean $stop$ to implement a start/stop function of \ref{SLR4}. The channel variables are used for communication between the different automata. 
	
	\begin{algorithm}
	\begin{algorithmic}[1]
		\State clock x;
		\State clock y;
		\State int[0,2] sensor = 0;
		\State int delay = 1;
		\State bool stop=true;
		\State broadcast chan push;
		\State broadcast chan fault;
		\State broadcast chan halt;
		\State chan release;
	\end{algorithmic}
	\end{algorithm}

    \subsection{The main automaton}
    \begin{figure}[h]
		\centering
		\includegraphics[scale=.65]{../includes/Main}
		\caption{The main automaton for our machine}
        \label{fig:UPPAAL model:Main}
	\end{figure}

    Our main automaton shown in figure \ref{fig:UPPAAL model:Main} uses the input of the sensor to determine which way to turn, according to \ref{SR1}, and also uses the boolean variable $stop$ which is changed in the on/off automaton to determine whether if it can proceed, according to \ref{SR2}. In a run it will get a sensor value and turn for a certain amount of time, where a disc is sorted. Since the $get\_sensor$ state is an urgent state, meaning no time passes until a transition is made, it is made sure that the system either immediately continues when $stop$ is false, or goes to the $idle$ state when $stop$ is true. The $calculating$ state is also urgent, and makes the transition to one of the two turn states, when the sensor value corresponds to either a black or white discs. When no disc is seen, it goes to a $waiting$ state, where it will do a delay and check the sensor value again. When no value is seen again, we can assume no more disc is present and the machine goes back to the $idle$ state, according to \ref{SLR2}. When taking this transition a synchronization message is send to the on/off automaton to turn off. \\
    When in a turn state, the machine turns for a given time period, called delay in this model. When an error occurs while in a turn state, an immediate transition is made to the $idle$ state, in order to have the machine stop turning. \\
    While in the $idle$ state and when $stop$ is true, thus when the machine is not running, a transition to the $calibration$ state can be made. In this state the calibration is done according to \ref{F1}.

    \newpage

    \subsection{The on/off automaton}
    \begin{figure}[h]
		\centering
		\includegraphics[scale=.6]{../includes/OnOff}
		\caption{The on/off automaton}
        \label{fig:UPPAAL model:OnOff}
	\end{figure}

    The on/off automaton shown in figure \ref{fig:UPPAAL model:OnOff} starts the machine when it gets input of a released button and sets $stop$ to false, such that the main automata can start. After another release the automata goes to the $pause$ state and sets $stop$ to true, such that the main automata will make no more turns. When in the paused state a button press is recorded, the model will go to the pressed state and it will start a timer. When the button is released within 2s it returns to the $on$ state, if the button is not released within two seconds the machine will turn off when the button is released. This is according to \ref{SR4}.
    
    \newpage
    
    \subsection{The user automaton}
    \begin{figure}[h]
        \centering
		\includegraphics[scale=1.5]{../includes/User}
		\caption{The user}
        \label{fig:UPPAAL model:User}
	\end{figure}

    The automaton shown in figure \ref{fig:UPPAAL model:User} deals with the user interaction, namely the push and release of a button. When a transition is made, a synchronization is proceeded to the on/off automaton where it will be handled. We do not implement this automaton in our software, rather it is used as a simulation for the pressing of the start/stop button.

    \subsection{The error automaton}
    \begin{figure}[h]
        \centering
		\includegraphics[scale=1.5]{../includes/Error}
		\caption{The error state}
        \label{fig:UPPAAL model:Error}
	\end{figure}

    Whenever errors occur, the error automaton shown in figure \ref{fig:UPPAAL model:Error} goes to the $error$ state, thus no transition can be made to any automaton until the error is resolved. It sends a synchronization which is used to stop turning when the main automaton is in a turn state. When the error is resolved, it goes back to the $no\ error$ state and the machine can continue.