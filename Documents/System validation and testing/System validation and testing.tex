\section{Introduction}
During the process of designing and building our machine, we made several tests and reviews. The purpose of these tests and reviews were to ensure that our final product met the requirements we set in section \ref{SLR}. Through code reviews, test cases and formal proofs, we tried to test and proof that the machine did what was asked at the beginning. We also need to make sure that the implementation does not do more than expected, so called 'unintended functionality', or contain 'silent' code that is (almost) never executed. \\
In the following section we will show that using the tests and proofs done we can conclude that the system level requirements hold and the machine is programmed as careful as possible. The reviews, tests and proofs used are found in the Appendix, section \ref{appendix}.

\section{Fulfilling system level requirements}
In order to assure the quality of this our machine we designed some tests that we can use to determine the quality and integrity of our machine, but these tests can also be used for the end-user in order to make sure that the machine is up to its quality's standards. These tests can be found in \ref{Machine test cases}. \\
In order to assure \ref{SLR1} we created tests \ref{TC1} and \ref{TC2}. These tests test both the normal behavior of the machine as well as using the machine with more than 12 discs. \\
In order to assure \ref{SLR2} we can again use \ref{TC1} as this simulates the normal behavior of the machine and the sorting process. The finishing after a "no disc" value is read is correctly executed here. \\
In order to assure \ref{SLR3} we created \ref{TC5}. In this test we make sure that all the wires from the Arduino can be disconnected one at a time as well as the speaker. This test also makes sure that the LCD will give proper feedback to the user. \\ 
In order to assure \ref{SLR4} we can again use \ref{TC1}. In this test the functionality of the start/stop button is tested to make sure that the machine will indeed abort when the users presses the button in the paused state for longer than 2 seconds. \\

With these tests in place we can assure that our machine fulfills the system level requirements.


\section{Traceability of end product}
To provide a convincing proof that we designed the sorting machine as careful as possible, we will show the traceability between the final code and the requirements. We will argue that all lines of code of our current program are necessary to accomplish the requirements and features we set for our machine. To show this, we will show the traceability between each phase, such that at the end we can conclude the traceability between the requirements and the final code.

\subsection{Traceability of specification and requirements}
In the software specification, in section \ref{SoftSpec}, we designed a state machine that would be the basis for the software design and implementation, i.e. the pseudocode and the object code. Thus this state machine is extremely important for the design of our final product. The implementation of all requirements and features are present except \ref{F2}, which is a not included in this level of abstraction. \\
Furthermore, we held tests cases in UPPAAL, which can be found in section \ref{UPPAAL tests}. We also proved some properties in a formal style in section \ref{formal proofs}. These tests and properties are used to verify claims.

\subsubsection{Global variables}
We start by arguing the variables used in the model are necessary for implementing the requirements and features. \\
The global variables are shown in subsection \ref{Declaration}. The clock $x$, int $sensor$ and $delay$ are used for implementing \ref{SLR1} and \ref{F4}. These variables are needed to have a working run in the main automaton. To correctly sort any number of discs, we need such a run to work, thus these variables are needed, since every run a disc is sorted. \\
The clock $y$, boolean $stop$, and channels $push$ and $halt$ are used for implementing  \ref{SLR4}. These variables are used to implement the response of the model to pushing of the start/stop button. \\
The channel $halt$ and, again, the int $sensor$ are used to implement \ref{SLR2}. When the sensor value reads 2, no disc, the model will use the channel $halt$ to stop the machine.\\
The channel $fault$ is used for implementing \ref{SLR3} and \ref{F3}.

\subsubsection{States and transitions}
Furthermore, we will argue that the states and transitions in the different automata are needed for implementing the requirements. \\

The main automaton is mainly needed for implementing \ref{SLR1}, \ref{SLR2}, \ref{F1} and \ref{F4}. The requirements \ref{SLR1} and \ref{F4} are accomplished by the states $idle$, $get\_sensor$, $calculating$, $turn\_left$ and $turn\_right$ and the transition between those states. These states are used to implement a run, where either a black or white disc is sorted. This run is seen as a loop starting and ending in $get_sensor$. \\
To implement \ref{SLR2} we use the states $waiting$ and $get\_disc$ and the transitions between these states and the states $calculating$, $get\_sensor$ and $idle$. These states are visited when the sensor gets a "no disc" value. They are needed to implement a small wait time after a no disc value is seen, to ensure the value has not changed after a small delay. \\
The feature \ref{F1} was added in the form of the state $calibration$. The transitions from and to this state are also needed to accomplish this. \\
The $fault$ transition is also needed for implementing \ref{SLR3} and \ref{F3}, while $halt$ is used for \ref{SLR4}.

The start/stop and user automata are both entirely needed to implement the requirement \ref{SLR4}. The user automaton is just a representation of the user pressing and releasing a button. The
start/stop automaton uses four states to implement the requirements. \textit{off}, $on$ and $pause$ are the different modes the machine can be in, while $pressed$ is needed to determine how long the button is pressed for either resuming or aborting. \\

The error automaton is needed for implementing the requirement \ref{SLR3} and \ref{F3}. This automaton is a very abstract implementation of this requirement, since you only have an error or no error state. \\

\begin{tabular}{|p{3cm}|p{6cm}|p{6cm}|}
	\hline
	\textbf{Requirement} & \textbf{States} & \textbf{Variables} \\ \hline
	\ref{SLR1} & $idle$, $get\_sensor$, $calculating$, $turn\_left$ and $turn\_right$ & Clock $x$, int $sensor$ and $delay$ \\ \hline
	\ref{SLR2} & $waiting$ and $get\_disc$ & Channel $halt$ and the int $sensor$ \\ \hline
	\ref{SLR3} & $error$ and $no\_error$ & Channel $fault$ \\ \hline
	\ref{SLR4} & \textit{off}, $on$, $pause$ and $pressed$ & Clock $y$, boolean $stop$, and channels $push$ and $halt$ \\ \hline
\end{tabular}

\subsubsection{Tests and proofs}
To prove that the requirements and features hold we held several tests and proved properties.\\

In the first UPPAAl test case, in subsection \ref{Ut1}, we tested the running and finishing of the machine and the starting and stopping function. Here we found that the state machine behaves as we wanted according to \ref{SLR1}, \ref{SLR2} and \ref{SLR4}.\\
In the second test, in subsection \ref{Ut2}, we tested whether the machine could respond correctly to errors occurring in various states. The machine responded as we expected, according to \ref{SLR3}. \\

In \ref{Property 1}, we proved that when the machine goes to a pause state in the on/off automaton the machine will stop after a time longer than $delay$. This means that the machine will pause after it has finished its last run, which at most takes $delay$ amount of time. This property shows that the pause function of \ref{SLR4} holds, with as detail that it takes at most $delay$ amount of time. \\
In \ref{Property 2}, we proved that whenever an error has occurred, the motor is not turning. This is essential for the error handling, as stated in the \ref{SLR3}. \\
In \ref{Property 3}, we proved that whenever a "no\_disc" value is found, and it remains after $delay$ amount of time, the machine is halted. This is the requirement of \ref{SLR2}.


\subsection{Traceability of pseudocode and specification}
In the software design, in section \ref{SoftDes}, we designed the pseudocode that would be the used for the software implementation, i.e. the object code. Thus this code is important for the design of our final product. \\
The different automata in the specification are implemented in the pseudocode in different ways. Some states have a corresponding variable, while others automata are implemented as interrupts. \\
Furthermore, we held code reviews in our group, which can be found in section \ref{code reviews}. These reviews are done to ensure the quality of the pseudocode.

\subsubsection{Main loop \& setup}
The main loop and setup, algorithms \ref{setup} and \ref{main loop algorithm}, show the biggest similarity to the main automaton and the on/off automaton. \\

In the setup we find the setting of pins, which is not present in the abstract specification, but needed for the Arduino to operate correctly. The variables $finished$, $paused$, $abort$ and $calibrating$ are all variables that correspond to states in the specification. Also a sensor interval is given, which is more specific than the abstract sensor values 0,1,2 in the specification.\\

In the main while loop of the machine a check for errors is done. In this call the error automaton of the specification is implemented. A sensor value is read and the machine turns accordingly. This is an implementation of a run in the main automaton in the specification. Also in this code different states in the on/off automaton display a specific message to the LCD, this is an addition compared to the specification. 

\subsubsection{Interrupts}
In the interrupts button presses are recorded. \\

The pause button presses are handled in the $PauseButtonPressed$ interrupt. In the specification this is done in the user and on/off automaton. This code is very similar to the specification, where depending on the state the machine is in a certain action is taken on a release of the button. The clock $y$ is implemented using the $timeStamp$ and $deltaTime$ variables. \\

In the $CalibrationButtonPressed$ interrupt a button press of the calibration button is recorded. This button was not modeled in the specification, where only a calibration state was present. To implement the calibration in this calibration state, we made use of a calibration button to make the transition from $idle$ to $calibration$ and back. When the calibration is made, the $emptyValue$ is read. This is used to determine disc values from the sensor input, abstractly represented in the specification as a 0,1,2 value of $sensor$.

\subsubsection{Helper functions}
In the helper functions, different functions are listed, necessary in the main program for correct functioning of the machine. \\

In the error detection function, algorithm \ref{Error detection algorithm}, the implementation of the transition to the $error$ state in the error automaton is made. Here for every pin a check for errors is made. The committed nature of the $error$ state is implemented by a while loop that is only exited when the error is resolved. In the specification this is when the transition is made back to the $no\_error$ state. \\

The motor controller function, algorithm \ref{setting motor algorithm}, is an implementation of the turn states in the specification. This function is used to set the motor to turn accordingly. \\

In the sensor voltage reading function, algorithm \ref{setting motor algorithm}, the sensor value is read and converted accordingly. This is an implementation of the $sensor$ variable in the specification.

\subsubsection{Code reviews}
To ensure the quality of the pseudocode we also held two code review sessions. Here we made sure the code was kept in line with the specification and that no unnecessary lines were present. We held one software walkthrough, where the lead programmer walked the rest of the group through the pseudocode. Here the relation between the specification and the pseudocode was a criteria that was checked thoroughly. In the pair programming session the cleanness of the code was checked, where two people looked over the code and also checked the correctness. Thus we ensured the careful design of the pseudocode.

\subsection{Traceability of C code and pseudocode}
In the software implementation, in section \ref{SI}, we designed the C code that would be used for the final implementation in the sorting machine. This code shows many similarities with the pseudocode, as the difference in abstraction is small. The C code has additions on a few parts, and has elaborations on the pseudocode. \\
The main additions on the pseudocode are that variables, such as the pins, have an actual value, the display and pins are set in detail and some functions have been added. \\

The main addition from the pseudocode is the addition of the "button debouncing" for the button interrupts of calibration and start/stop button. This is talked about in subsection \ref{debouncing}. The debouncing is an addition to get the button interrupts in the pseudocode to work. Thus it is a necessary addition to the C code. \\

The C code has the same layout as the pseudocode. It starts with declaring variables, then the initial setup is made. Additions are the details on pins and such, necessary for operating the Arduino. The main while loop of the program runs through the same basic steps as the pseudocode, with the addition of prints to the LCD and delays for practical purposes. \\
The interrupts for calibration and the start/stop button are present like in the pseudocode. The helper functions are also implemented in the C code, with the addition of a few more functions. \\
The $writeTime()$ function deals with writing the elapsed time on the LCD screen, which is an elaboration on the things the LCD displays during a run. The function $setPinMode()$ and $isValidInterrupt( int \ pin )$ are used for the implementation of the detect error function. The $setInterrupt(bool \ enable)$ function is used to enable the two interrupts.

\section{Conclusion}
We have argued about the validity of our final product in this chapter. Through the use of test cases, code reviews and formal proofs we have tried to show that the final machine behaves as wanted. The system level requirements are shown to be true in the section, with the help of the machine test cases. Furthermore, we have shown the traceability between each phase of the project, and thus the traceability between the final code and the system level requirements. By showing this traceability we have argued about the addition and implementation in each phase, and shown the reasoning behind it. Thus we have shown that we have carefully designed the sorting machine.